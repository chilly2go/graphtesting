/**
 * @author Marcel
 */

import graph.Dijkstra;
import graph.Graph;
import graph.Kante;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Erzeugen der Graphenstruktur
 */
public class Graphenstruktur
{
  public static final String            GRID         = "GRID";
  public static final String            GRID_TESTING = "GRID2";
  public static final String            SPIDER       = "SPIDER";
  public static final String            LADDER       = "LADDER";
  public              ArrayList<String> graphAList; // ArrayList
  String         graph; // Testzwecke
  Stream<String> graphstream; // Stream zum Einlesen
  private Graph graphMap;
  
  public Graphenstruktur(String graph)
  {
    this.graphMap = new Graph();
    String resName = null;
    if (graph.equals("SPIDER"))
    {
      this.graph = "SPIDER";
      resName = "Ressources/SPIDER";
    }
    else if (graph.equals("LADDER"))
    {
      this.graph = "LADDER";
      resName = "Ressources/LADDER";
    }
    else if (graph.equals("GRID"))
    {
      this.graph = "GRID";
      resName = "Ressources/GRID";
    }
    else if (graph.equals("GRID2"))
    {
      this.graph = "GRID2";
      resName = "Ressources/GRID2";
    }
    try
    {
      graphstream = Files.lines(Paths.get(resName));
      graphAList = new ArrayList<>();
      graphstream.parallel().forEachOrdered(graphAList::add);
    } catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * public String read(){ return graph; }
   */
  
  //    /** Ausgeben der Textdateien */
  //    public Stream<String> alleZeilenAusgeben(){
  //        //graphstream.forEach(System.out::println);
  //        graphstream.forEach(line -> {
  //            System.out.println(line);
  //        });
  //        return graphstream;
  //    }
  
  //    /** Ausgeben der Textdateien als ArrayList*/
  //    public void alleZeilenAusgeben2(){
  //        for (String line : graphAList){
  //            System.out.println(line);
  //        }
  //    }
  public static void main(String[] args)
  {
    //        Graphenstruktur grSpider = new Graphenstruktur("SPIDER");
    //        System.out.println("Ausgabe Spider-Datensatz");
    //        grSpider.alleZeilenAusgeben2();
    //
    //        Graphenstruktur grLadder = new Graphenstruktur("LADDER");
    //        System.out.println("Ausgabe Ladder-Datensatz");
    //        grLadder.alleZeilenAusgeben2();
  
    Graphenstruktur grGrid = new Graphenstruktur(Graphenstruktur.SPIDER);
    grGrid.arrayListErstellen();
    // System.out.println("Ausgabe Grid-Datensatz");
    // grGrid.alleZeilenAusgeben2();
    Dijkstra dij = new Dijkstra(grGrid.graphMap);
    dij.work(1);
    dij.printTree(1, 996);
    dij.printTree(1, 996, false);
    //    grGrid.alleZeilenDerListeAusgeben();
    //System.out.print(gr.read());
    // Als Beispiel: graph.Dijkstra dij = new graph.Dijkstra();
  }
  
  /**
   * Erstellen der ArrayList mit einzelnen Einträgen vom Typ valueList
   */
  public void arrayListErstellen()
  {
    for (String line : this.graphAList)
    {
      String[] tmp = line.split("\\s+");
      if (tmp.length == 3)
      { this.graphMap.neueKante(tmp[0], tmp[1], tmp[2]); }
    }
    this.graphAList.clear();
  }
  
  /**
   * Für mich zum Ausgeben
   */
  public void alleZeilenDerListeAusgeben()
  {
    System.out.println("Als Liste");
    System.out.println("Kanten: " + this.graphMap.kanten.size());
    System.out.println("graph.Knoten: " + this.graphMap.knoten.size());
    for (Kante kante : this.graphMap.kanten)
    {
      System.out.println(kante + " ");
    }
  }
}

