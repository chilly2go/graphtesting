package graph.util;

import graph.Kante;

/**
 * Spezielle Liste, welche die Kanten beim einfügen sortiert
 */
public class GraphWeightsortedEdgeList
{
  private GraphWeightsortedEdgeList next;
  private Kante                     kante;
  
  /**
   * leere Liste anlegen
   */
  public GraphWeightsortedEdgeList()
  {
  
  }
  
  /**
   * neue Liste mit Startwert (Startkante) anlegen
   *
   * @param kante
   */
  public GraphWeightsortedEdgeList(Kante kante)
  {
    this.kante = kante;
  }
  
  /**
   * Fügt eine neue Kante der Liste hinzu. Die neue Kante wird beim einfügen nach Gewicht einsortiert
   *
   * @param kante
   *
   * @return
   */
  public GraphWeightsortedEdgeList add(Kante kante)
  {
    //    nicht initialisiert? kante speichern
    if (this.kante == null)
    { this.kante = kante; }
    else
      //    liste soll sortiert sein.
      //    neue kante mit kleinerem gewicht? aktuelle kante nach hinten verschieben
      if (this.kante.gewicht > kante.gewicht)
      {
        if (this.next == null)
        { this.next = new GraphWeightsortedEdgeList(this.kante); }
        else
        { this.next.add(this.kante); }
        this.kante = kante;
      }
      //    neue kante mit gewicht größer gleich aktueller kante? hinten anstellen
      else
      {
        if (this.next == null)
        { this.next = new GraphWeightsortedEdgeList(kante); }
        else
        { this.next.add(kante); }
      }
    return this;
  }
  
  /**
   * Nächsten Wert abholen
   *
   * @return Nächste Kante oder null, wenn leer
   */
  public Kante getNext()
  {
    // wir haben keinen eintrag mehr? return null
    if (this.kante == null) { return null; }
    else
    {
      // aktuellen wert zwischen speichern
      Kante tmp = this.kante;
      // weitere einträge in der liste?
      if (this.next != null)
      {
        // liste "aufrutschen" lassen
        this.kante = this.next.kante;
        this.next = this.next.next;
      }
      else
      {
        // jetzt ist ende :)
        this.kante = null;
      }
      return tmp;
    }
  }
  
  /**
   * Weitere Werte verfügbar?
   *
   * @return Boolean
   */
  public boolean hasNext()
  {
    // solange der eigene wert nicht null ist, gibt es noch etwas zu holen
    return this.kante != null;
  }
}
