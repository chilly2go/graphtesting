package graph;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph
{
  public ArrayList<Kante>         kanten;
  public HashMap<Integer, Knoten> knoten;
  
  public Graph()
  {
    this.kanten = new ArrayList<>();
    this.knoten = new HashMap<Integer, Knoten>();
  }
  
  public Graph neueKante(String start, String gewicht, String ende)
  {
    return this.neueKante(Integer.valueOf(start), Integer.valueOf(gewicht), Integer.valueOf(ende));
  }
  
  public Graph neueKante(int start, int gewicht, int ende)
  {
    if (!this.knoten.containsKey(start))
    { this.knoten.put(start, new Knoten(start)); }
    if (!this.knoten.containsKey(ende))
    { this.knoten.put(ende, new Knoten(ende)); }
    Knoten startKnoten, endKnoten;
    Kante  kante;
    startKnoten = this.knoten.get(start);
    endKnoten = this.knoten.get(ende);
    kante = new Kante(startKnoten, gewicht, endKnoten);
    startKnoten.neueKante(kante);
    endKnoten.neueKante(kante);
    this.kanten.add(kante);
    return this;
  }
  
  public Knoten getKnoten(int knoten)
  {
    if (this.knoten.containsKey(knoten))
    { return this.knoten.get(knoten); }
    else
    { throw new IndexOutOfBoundsException("graph.Knoten existiert nicht"); }
  }
}
