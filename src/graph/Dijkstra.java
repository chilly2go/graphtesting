package graph;

import graph.util.GraphWeightsortedEdgeList;

import java.util.HashSet;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * graph.Dijkstra graph.Graph Algorithmus
 * <p>
 * Ablauf:
 * <p>
 * -Start graph.Knoten -Ausgehend vom aktuellen graph.Knoten die günstigste Verbindung suchen
 */
public class Dijkstra
{
  
  Knoten                      current;
  HashSet<Integer>            visited;
  LinkedBlockingQueue<Knoten> queue;
  private Graph graph;
  
  public Dijkstra(Graph graphMap)
  {
    this.graph = graphMap;
    this.queue = new LinkedBlockingQueue<>();
  }
  
  /**
   * Run Dijkstra starting with the provided starting node
   *
   * @param startKnoten
   */
  public void work(int startKnoten)
  {
        /*
        hole startknoten aus graph
        aktueller knoten = start knoten
        loop:
                aktueller knoten
                    -> betrachte alle verbindungen
                    -> (knoten in warteschlage legen, sortiert nach gewicht)
                    -> wegkosten an betrachtetem knoten (jeweils) speichern. wegkosten = bisherige kosten +
                    verbindungs-gewicht
                aktueller knoten -> als bearbeitet markieren
                aktueller knoten = erster eintrag der warteschlage
         */
    this.visited = new HashSet<>();
    // get and set starting node
    this.current = this.graph.getKnoten(startKnoten).setIsStartKnoten();
    // as long as there are nodes to handle. do so :)
    while (this.current != null)
    { this.traverseNodes(); }
    System.out.println("dijkstra done");
  }
  
  /**
   * checking connections of current node finding the cheapest ways
   */
  private void traverseNodes()
  {
    GraphWeightsortedEdgeList list = new GraphWeightsortedEdgeList();
    // betrachte alle ausgehenden verbindungen des aktuellen knotens
    for (Kante kante : this.current.ausgehend)
    {
      // no edges with target nodes already visited
      if (!this.visited.contains(kante.ende.getNummer()))
      {
        if (kante.ende.getKosten() < 0)
        {
          throw new IllegalArgumentException(
              "Kosten kleiner 0 sind für den Dijkstra-Algorithmus nicht zulässig: " + kante);
        }
        // make a list of our edges (ordered by weight)
        list.add(kante);
        // check it out ;)
        kante.ende.visitNode(kante);
      }
    }
    // for all "checked" edges
    while (list.hasNext())
    {
      Kante tmp = list.getNext();
      // add to queue if neither end if the edge has been visited before and node is not already queued
      if (!this.visited.contains(tmp.start.getNummer()) || !this.visited.contains(tmp.ende.getNummer()))
      {
        if (!this.queue.contains(tmp.ende))
        { this.queue.add(tmp.ende); }
      }
    }
    // add current node to list of already visited nodes
    this.visited.add(this.current.getNummer());
    // we got more to do?
    if (this.queue.size() > 0)
    {
      // get next node to handle
      this.current = queue.remove();
    }
    else
    { this.current = null; }
  }
  
  public void printTree(int startKnoten, int endKnoten)
  {
    this.printTree(startKnoten, endKnoten, true);
  }
  
  public void printTree(int startKnoten, int endKnoten, boolean withCost)
  {
    Knoten        current = this.graph.getKnoten(endKnoten);
    Stack<Knoten> stack   = new Stack<>();
    while (current.vorgaenger != null)
    {
      stack.push(current);
      current = current.vorgaenger.start;
    }
    stack.push(current);
    System.out.println("Dijsktra Weg von Knoten " + startKnoten + " zu " + endKnoten);
    while (!stack.empty())
    {
      if (withCost)
      { System.out.print(stack.pop()); }
      else
      { System.out.print(stack.pop().getNummer()); }
      if (!stack.empty()) System.out.print(" - ");
    }
    System.out.println();
  }
}
