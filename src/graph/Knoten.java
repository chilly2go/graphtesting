package graph;

import java.util.ArrayList;

/**
 * Node class representing a node within a graph. Handling connections from / to other nodes, a cost to reach this node
 * and a reference to the previous node
 */
public class Knoten
{
  public static final boolean          isDirected = false;
  public              ArrayList<Kante> eingehend;
  public              ArrayList<Kante> ausgehend;
  private             int              nummer;
  
  /**
   * To discern nodes we require a number for identification
   *
   * @param nummer
   */
  public Knoten(int nummer)
  {
    this.nummer = nummer;
    // nodes start with a high cost. makes it easier to get nodes not yet checked (or unreachable?)
    this.kosten = Integer.MAX_VALUE;
    // initialise lists for in-/outbound edges
    this.eingehend = new ArrayList<>();
    this.ausgehend = new ArrayList<>();
  }
  
  /**
   * Get the identifying number for this node
   *
   * @return
   */
  public int getNummer()
  {
    return this.nummer;
  }
  
  public int   kosten;
  public Kante vorgaenger;
  
  /**
   * Add an edge to this node. Proper handling for in-/outbound connections (directed graph or not) included
   *
   * @param kante
   *     the edge to add to this nodes lists
   *
   * @return
   */
  public Knoten neueKante(Kante kante)
  {
    // working with directed graph?
    if (Knoten.isDirected)
    {
      // edges only go one way so we have to store only that
      if (kante.start.equals(this))
      { this.ausgehend.add(kante); }
      else
      { this.eingehend.add(kante); }
    }
    else
    {
      // edges go both ways. so determine if this node is start or end of the connection
      Kante in, out;
      if (kante.start.equals(this))
      {
        // connection start. store reverse edge for incoming
        out = kante;
        in = new Kante(kante.ende, kante.gewicht, kante.start);
      }
      else
      {
        // connection end. store reverse edge for out going
        in = kante;
        out = new Kante(kante.ende, kante.gewicht, kante.start);
      }
      this.ausgehend.add(out);
      this.eingehend.add(in);
    }
    return this;
  }
  
  /**
   * when a starting node is provided it has no predecessor and no starting cost
   *
   * @return
   */
  public Knoten setIsStartKnoten()
  {
    this.vorgaenger = null;
    this.setKosten(0);
    return this;
  }
  
  /**
   * Check node against current edge. Update cost and predecessor if applicable
   *
   * @param edge
   *     the edge to check against
   *
   * @return
   */
  public Knoten visitNode(Kante edge)
  {
    return this.updateKosten(edge);
  }
  
  /**
   * Check node against current edge. Update cost and predecessor if applicable
   *
   * @param kante
   *     the edge to check against
   *
   * @return
   */
  public Knoten updateKosten(Kante kante)
  {
    // no predecessor? Not visited before! Lets start.
    if (this.vorgaenger == null)
    {
      this.setVorgaenger(kante);
    }
    else
    {
      // calc cost of this new connection
      int neueKosten = kante.getKosten();
      // new cost is better? (smaller) -> update
      if (neueKosten < this.getKosten())
      {
        this.setVorgaenger(kante);
      }
    }
    return this;
  }
  
  /**
   * Found a new predecessor? Store it and update cost for this node.
   * <p>
   * Only update if edge leads to this node. (it will have no effect if falsely called)
   *
   * @param kante
   *     edge providing new predecessor
   *
   * @return
   */
  public Knoten setVorgaenger(Kante kante)
  {
    if (kante.ende.equals(this))
    {
      this.setKosten(kante.start.getKosten() + kante.gewicht);
      this.vorgaenger = kante;
    }
    return this;
  }
  
  /**
   * Cost traveling to this node
   *
   * @return
   */
  public int getKosten()
  {
    return kosten;
  }
  
  /**
   * Update cost of this node
   *
   * @param kosten
   *
   * @return
   */
  public Knoten setKosten(int kosten)
  {
    this.kosten = kosten;
    return this;
  }
  
  @Override
  public String toString()
  {
    return this.getNummer() + "(Kosten: " + this.getKosten() + ")";
  }
}
