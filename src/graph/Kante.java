package graph;

/**
 * Edge
 * <p>
 * Representation of a connection between 2 nodes including a cost of using said connection
 */
public class Kante
{
  public       Knoten start;
  public final int    gewicht;
  public       Knoten ende;
  
  /**
   * create a new edge
   *
   * @param start
   *     start node
   * @param gewicht
   *     weight of this edge / cost of travling or moving along this edge
   * @param ende
   *     end node
   */
  public Kante(Knoten start, int gewicht, Knoten ende)
  {
    this.start = start;
    this.gewicht = gewicht;
    this.ende = ende;
  }
  
  /**
   * get the cost of traveling from starting node to end node via this edge
   *
   * @return start node cost + edge weight
   */
  public int getKosten()
  {
    return this.start.getKosten() + this.gewicht;
  }
  
  @Override
  public String toString()
  {
    return this.start.getNummer() + " - " + this.gewicht + " - " + this.ende.getNummer();
  }
}
